# USB device switcher for QEMU

Tool for managing which USB devices are passed through to a QEMU VM.

This tool uses the QMP interface provided by QEMU.


## TODO:
- Find and list available USB devices dynamically
- Use a more light weight UI
- Refactor thread communication

