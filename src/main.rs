extern crate json;

mod gui;
mod commander;

use std::sync::{ Arc, Mutex };
use commander::Commander;

fn main()
{
	let commander = Arc::new(Mutex::new(Commander::new()));

	gui::tykitys(commander.clone());

	println!("bye bye");
}
