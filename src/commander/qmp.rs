use std::sync::{ Arc, Mutex };
use std::io::Read;
use std::io::Write;
use std::time::Duration;
use std::collections::VecDeque;

pub struct QMPCommand
{
	execute: json::JsonValue,
	result: json::JsonValue,
	error: json::JsonValue
}

impl QMPCommand {
	pub fn new(execute: json::JsonValue) -> QMPCommand
	{
		return QMPCommand {
			execute: execute,
			result: json::Null,
			error: json::Null
		}
	}
}

pub fn start_qmp_thread(
	queue: Arc<Mutex<VecDeque<QMPCommand>>>,
	results: Arc<Mutex<VecDeque<QMPCommand>>>
) -> std::thread::JoinHandle<()>
{
	std::thread::spawn(move || {
		qmp_thread(queue, results)
	})
}

fn qmp_thread(
	shared_command_queue: Arc<Mutex<VecDeque<QMPCommand>>>,
	shared_results: Arc<Mutex<VecDeque<QMPCommand>>>
)
{
	enum ConnectPhase
	{
		WaitingGreeting,
		WaitingCapabilitiesNegotiation,
		Ready,
		Quit
	}

	let mut stream = std::net::TcpStream::connect("localhost:7009")
		.unwrap_or_else(|e| { panic!("Connection failed: {}", e) });

	println!("Connected!");

	stream.set_read_timeout(Some(Duration::from_millis(500)))
		.expect("Setting socket timeout failed!");

	let mut phase = ConnectPhase::WaitingGreeting;
	let mut wait_reply = false;

	// let mut command_queue: VecDeque<QMPCommand> = VecDeque::new();
	
	// command_queue.push_back(QMPCommand::new(json::object!{ "execute": "query-status" }));

	// command_queue.push_back(QMPCommand::new(json::object!{ "execute": "query-commands" }));

	// command_queue.push_back(QMPCommand::new(json::object!{
	// 	"execute": "device_del",
	// 	"arguments": json::object!{
	// 		"id": "snow"
	// 	}
	// }));

	/*
	command_queue.push_back(QMPCommand::new(json::object!{
		"execute": "device_add",
		"arguments": json::object!{
			"usb-host": json::Null,
			"id": "nappis",
			"vendorid": "",
			"productid": ""
		}
	}));
	*/

	while !matches!(phase, ConnectPhase::Quit)
	{
		println!("qmp tick");

		let mut reply_raw = String::new();
		stream.read_to_string(&mut reply_raw).ok();

		let mut command_queue = shared_command_queue.as_ref().lock().unwrap();
		println!("command queue has {} commands waiting", command_queue.len());

		println!("Got a message (size: {size}): {payload}", size = reply_raw.len(), payload = reply_raw);

		let reply_obj = json::parse(reply_raw.as_ref()).unwrap_or(json::JsonValue::Null);

		// Read reply

		if !reply_obj.is_object()
		{
			if reply_raw.len() != 0
			{
				println!("Got something but it was not a valid JSON object.");
			}
		}
		else if !reply_obj["return"].is_null()
		{
			let mut cmd = command_queue.pop_front().expect("Command queue was empty but still got a reply.");

			println!("Ok: {}", reply_raw);

			if matches!(phase, ConnectPhase::WaitingCapabilitiesNegotiation)
			{
				phase = ConnectPhase::Ready;
			}

			cmd.result = reply_obj;

			shared_results.as_ref().lock().unwrap().push_back(cmd);

			wait_reply = false;
		}
		else if !reply_obj["error"].is_null()
		{
			let mut cmd = command_queue.pop_front().expect("Command queue was empty but still got a reply.");
			cmd.result = reply_obj;

			shared_results.as_ref().lock().unwrap().push_back(cmd);

			println!("Error: {}", reply_raw);
			phase = ConnectPhase::Quit;
			wait_reply = false;
		}
		else if !reply_obj["QMP"].is_null()
		{
			println!("Got greeting");

			if !matches!(phase, ConnectPhase::WaitingGreeting)
			{
				panic!("Got greeting when in wrong connection phase!");
			}

			// Got greeting message
			// Negotiate capabilities
			command_queue.push_front(QMPCommand::new(json::object!{
				"execute" => "qmp_capabilities"
			}));

			wait_reply = false;
			phase = ConnectPhase::WaitingCapabilitiesNegotiation;
		}

		// Send next command
		if !wait_reply && !command_queue.is_empty()
		{
			let &cmd = command_queue.get(0).as_ref().expect("err wut");
			
			println!("Sending message: {}", cmd.execute);

			stream.write(cmd.execute.to_string().as_bytes())
				.unwrap_or_else(|e| {
					println!("Sending command failed. Will quit now. Error was: {}", e);
					phase = ConnectPhase::Quit;
					return 0;
				}
			);

			// Wait for reply before executing more
			wait_reply = true;
		}
	}
}
