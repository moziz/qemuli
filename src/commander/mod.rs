mod qmp;

use std::sync::{ Arc, Mutex };
use std::collections::VecDeque;

pub struct Commander
{
	command_queue: Arc<Mutex<VecDeque<qmp::QMPCommand>>>,
	results: Arc<Mutex<VecDeque<qmp::QMPCommand>>>,
	pub qmp_thread_join_handle: std::thread::JoinHandle<()>,
	debugValue: u64
}

impl Commander
{
	pub fn new() -> Commander
	{
		let command_queue = Arc::new(Mutex::new(VecDeque::new()));
		let results = Arc::new(Mutex::new(VecDeque::new()));

		Commander {
			command_queue: command_queue.clone(),
			results: results.clone(),
			qmp_thread_join_handle: qmp::start_qmp_thread(command_queue.clone(), results.clone()),
			debugValue: 0
		}
	}

	fn enqueue_command(&mut self, command: qmp::QMPCommand)
	{
		self.command_queue.lock().unwrap().push_back(command);
	}

	pub fn device_add_usb(&mut self, id: &str, vendor_id: u32, product_id: u32)
	{
		self.enqueue_command(qmp::QMPCommand::new(json::object!{
			"execute": "device_add",
			"arguments": json::object!{
				"driver": "usb-host",
				"id": id,
				"vendorid": format!("{:#06x}", vendor_id),
				"productid": format!("{:#06x}", product_id)
			}
		}));

		println!("devide addd usb");
	}

	pub fn device_del(&mut self, id: &str)
	{
		self.enqueue_command(qmp::QMPCommand::new(json::object!{
			"execute": "device_del",
			"arguments": json::object!{
				"id": id
			}
		}));

		println!("devidee dellele");
	}

	pub fn get_debug_value(&mut self) -> u64
	{
		self.debugValue += 1;
		return self.debugValue;
	}
}
