use iced::button;
use iced::{
	Element, Button, Column, Text, Application, Command
};

use std::sync::{ Arc, Mutex };
use crate::Commander;

pub struct Flags
{
	commander: Arc<Mutex<Commander>>
}

struct MainView
{
	flags: Flags,

	value: i32,

	inc_button_state: button::State,
	dec_button_state: button::State
}

#[derive(Debug, Clone, Copy)]
enum Message
{
	IncPressed,
	DecPressed
}

impl Application for MainView
{
	type Executor = iced::executor::Null;
	type Message = Message;
	type Flags = Flags;

	fn new(flags: Flags) -> (MainView, Command<Message>)
	{
		(
			MainView {
				flags: flags,
				value: 0,
				inc_button_state: button::State::default(),
				dec_button_state: button::State::default()
			},
			Command::none()
		)
	}

	fn title(&self) -> String
	{
		String::from("Switcheroo ou jee")
	}

	fn view(&mut self) -> Element<Message>
	{
		Column::new()
		.push(
			Button::new(&mut self.inc_button_state, Text::new("Add device"))
			.on_press(Message::IncPressed)
		)
		.push(
			Text::new(&self.value.to_string()).size(50)
		)
		.push(
			Button::new(&mut self.dec_button_state, Text::new("Remove device"))
			.on_press(Message::DecPressed)
		)
		.push(
			Text::new(self.flags.commander.as_ref().lock().unwrap().get_debug_value().to_string()).size(50)
		)
		.into()
	}

	fn update(&mut self, message: Message) -> Command<Message>
	{
		match message
		{
			Message::IncPressed =>
			{
				self.value += 1;
				self.flags.commander.as_ref().lock().unwrap().device_add_usb("snow", 0x046d, 0x0ab9);
			}

			Message::DecPressed =>
			{
				self.value -= 1;
				self.flags.commander.as_ref().lock().unwrap().device_del("snow");
			}
		}

		Command::none()
	}
}

pub fn tykitys(commander: Arc<Mutex<Commander>>)
{
	MainView::run(iced::Settings::with_flags(Flags{ commander: commander }))
}
